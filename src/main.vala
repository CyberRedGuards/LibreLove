using Soup;

void default_handler (Soup.Server server, Soup.Message msg, string path,
                      GLib.HashTable? query, Soup.ClientContext client)
{
    string html_head = "<head><title>love</title><meta charset=\"utf-8\"></head>";
    string html_cont = "<h1>wir.su</h1><p><a href=\"/love/signin\">Demo</a></p>";
    string html_body = """<body>%s</body>""".printf (html_cont);

    msg.set_response ("text/html", Soup.MemoryUse.COPY,
                      "<html>%s%s</html>".printf (html_head, html_body).data);
}

void si_handler (Soup.Server server, Soup.Message msg, string path,
                 GLib.HashTable? query, Soup.ClientContext client)
{
    GLib.SList<Soup.Cookie> cookies = Soup.cookies_from_request (msg);
    foreach (Soup.Cookie cookie in cookies) {
        if (cookie.name == "cookie") {
            msg.set_redirect(307, "/love");
        }
	}

    Soup.Cookie cookie = new Soup.Cookie ("cookie", "test", "wir.su", "/", -1);
	SList<Soup.Cookie> list = new SList<Soup.Cookie> ();
	list.append (cookie);
    Soup.cookies_to_response(list, msg);
    string html_head = "<head><title>love</title><meta charset=\"utf-8\"></head>";
    string html_cont = "<h1>wir.su</h1><p>Успешно.</p>";
    string html_body = """<body>%s</body>""".printf (html_cont);

    msg.set_response ("text/html", Soup.MemoryUse.COPY,
                      "<html>%s%s</html>".printf (html_head, html_body).data);
}

void main () {
    var server = new Soup.Server (Soup.SERVER_PORT, 3000);
    server.add_handler ("/love", default_handler);
    server.add_handler ("/love/signin", si_handler);
    server.run ();
}